**BSWInfra** is a small collection of classes which can serve as base for Unix (BSD and Linux) application development.

## Components
The project can be compiled as shared library and provides the following major classes:

### Application
The main application class. If included, a Logger and a main event loop are constructed. The static application object can be easily accessed with app().   
### EventLoop
An event loop ensures that all sources will be executed serialized in the same thread context. The event loop has two priority queues `IEventLoop::Priority::Normal` (default) and `IEventLoop::Priority::High` (always executed before default).
The event loop can accept multiple pollable sources. The following common sources are implemented in the library:    
1. **Timer** A basic timer class
2. **UserEvent** User event source which can be triggered explicitly
3. **PathEvent** A file system notifier (Inotify, KEvent)
4. **AsyncQueue** Asyncronous generic queue 
5. **Pollable** Generic file descriptor pollable source
    
### UnixPath
A simple helper class to handle unix filesystem paths
    
### Logger
A simple logger class (singletone) with multiple logging system support (build time defined)
    
### KeyFile
An INI like file parser to read configuration (only reader, no emitter)
