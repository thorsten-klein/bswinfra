cmake_minimum_required(VERSION 3.7.2)
project(BaseSoftwareInfrastructure)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")
set(CMAKE_INCLUDE_CURRENT_DIR ON)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "RelWithDebInfo" CACHE STRING
        "Choose the type of build: Debug, Release, RelWithDebInfo, MinSizeRel." FORCE)
endif()

# Add support for coverage analysis
if(CMAKE_BUILD_TYPE STREQUAL Coverage)
    set(COVERAGE_EXCLUDES
        "*/demos/*"
        "*/build/*"
        "*/tests/*"
    )
    set(COVERAGE_BASE_DIR ${CMAKE_SOURCE_DIR}/source)
    set(COVERAGE_BRANCH_COVERAGE ON)
    set(COVERAGE_THRESHOLD_LINE 90)
    set(COVERAGE_THRESHOLD_FUNCTION 90)

    include(CoverageTarget)
endif()

option(WITH_TESTS "Build test suite" N)
option(WITH_DEMOS "Build demo binaries" N)
option(WITH_SYSLOG "Build Logger with syslog" N)
option(WITH_JOURNALD "Build Logger with journald (overwritten by syslog if enabled)" N)
option(WITH_DEBUG_DEPLOY "Deploy unit test to the target system" N)
option(WITH_TIDY "Build with clang-tidy" N)

if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
    option(WITH_JOURNALD "Build Logger with systemd-journal" N)
endif()

if(WITH_TIDY)
	set(CMAKE_CXX_CLANG_TIDY "clang-tidy")
endif()

if(WITH_SYSLOG AND WITH_JOURNALD)
    message(FATAL_ERROR "Cannot configure syslog and journald logger backend at the same time")
endif()

if(WITH_SYSLOG)
	add_compile_options("-DWITH_SYSLOG")
endif()

if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
    if(WITH_JOURNALD)
	    add_compile_options("-DWITH_JOURNALD")
    endif()
endif()

# Dependencies
find_package          (PkgConfig REQUIRED)
find_package		  (Threads REQUIRED)
set                   (LIBS ${LIBS} pthread)

# Build
add_subdirectory(source)

if(WITH_DEMOS)
    add_subdirectory(demos)
endif()

if(WITH_TESTS OR WITH_DEBUG_DEPLOY)
    enable_testing()
    add_subdirectory(tests)
endif()

# Build flags
add_compile_options (
    -Wall
    -Wextra
    -Wno-unused-function
    -Wformat
    -Wno-variadic-macros
    -Wno-strict-aliasing
    -D_FORTIFY_SOURCE=2
    -fstack-protector-strong
    -fwrapv
    -Wformat-signedness
    -Wmissing-include-dirs
    -Wimplicit-fallthrough=5
    -Wunused-parameter
    -Wuninitialized
    -Walloca
    -Wduplicated-branches
    -Wduplicated-cond
    -Wfloat-equal
    -Wshadow
    -Wcast-qual
    -Wconversion
    -Wsign-conversion
    -Wlogical-op
    -Werror
    -Wformat-security
    -Walloc-zero
    -Wcast-align
    -Wredundant-decls
    )

# Status reporting
message (STATUS "CMAKE_BUILD_TYPE: "     ${CMAKE_BUILD_TYPE})
message (STATUS "SYSTEM_TYPE: "          ${CMAKE_SYSTEM_NAME})
message (STATUS "WITH_SYSLOG: "          ${WITH_SYSLOG})
message (STATUS "WITH_JOURNALD: "        ${WITH_JOURNALD})
message (STATUS "WITH_TESTS: "           ${WITH_TESTS})
message (STATUS "WITH_DEMOS: "           ${WITH_DEMOS})
message (STATUS "WITH_TIDY: "            ${WITH_TIDY})
message (STATUS "WITH_DEBUG_DEPLOY: "    ${WITH_DEBUG_DEPLOY})

