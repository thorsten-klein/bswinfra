message(STATUS "Adding Base Software Infrastructure project")

option(WITH_SYSLOG "Build Logger with syslog" N)
if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
    option(WITH_JOURNALD "Build Logger with systemd-journal" N)
endif()

if(WITH_SYSLOG AND WITH_JOURNALD)
    message(FATAL_ERROR "Cannot configure syslog and journald logger backend at the same time")
endif()

if(WITH_SYSLOG)
	add_compile_options("-DWITH_SYSLOG")
endif()

if (${CMAKE_SYSTEM_NAME} STREQUAL Linux)
    if(WITH_JOURNALD)
	    add_compile_options("-DWITH_JOURNALD")
        find_package          (PkgConfig REQUIRED)
        pkg_check_modules     (LIBSYSTEMD libsystemd REQUIRED)
        include_directories   (${LIBSYSTEMD_INCLUDE_DIRS})
        set                   (LIBS ${LIBS} ${LIBSYSTEMD_LIBRARIES})
    endif()
endif()

# Status reporting
message (STATUS "   SYSTEM_TYPE: "          ${CMAKE_SYSTEM_NAME})
message (STATUS "   WITH_SYSLOG: "          ${WITH_SYSLOG})
message (STATUS "   WITH_JOURNALD: "        ${WITH_JOURNALD})

add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/../source)
