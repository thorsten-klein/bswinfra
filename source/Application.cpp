/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Application Class
 * @details   Basic event based application class
 *-
 */

#include "Application.h"

#include "Exceptions.h"

using std::shared_ptr;
using std::string;

namespace bswi::app
{

Application *Application::instance = nullptr;

Application::Application(const string &name, const string &description)
: IApplication(name, description)
{
  if (Application::instance != nullptr) {
    throw bswi::except::SingleInstance();
  }

  instance = this;
}

} // namespace bswi::app
