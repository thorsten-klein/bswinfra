/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQPollable Class
 * @details   KQueue based Pollable implementation
 *-
 */

#pragma once

#include "IPollable.h"
#include "KQTypes.h"

namespace bswi::event
{

class KQPollable : public IPollable
{
public:
  explicit KQPollable(const std::string &name,
                      const std::function<bool()> &callback,
                      int fd,
                      int events,
                      Priority priority = Priority::Normal);

  explicit KQPollable(const std::string &name)
  : IPollable(name)
  {
  }

  virtual ~KQPollable() = default;

  void
  lateSetup(const std::function<bool()> &callback, int fd, int events, Priority priority) final;

private:
  KEventDescriptor m_keventDescriptor = {};
};

} // namespace bswi::event
