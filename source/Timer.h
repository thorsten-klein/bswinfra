/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Timer Class
 * @details   Timer wrapper class
 *-
 */

#pragma once

#if defined(__linux__)
#include "EPTimer.h"
#else
#include "KQTimer.h"
#endif

namespace bswi::event
{

#if defined(__linux__)
class Timer : public EPTimer
{
public:
  using EPTimer::EPTimer;
};
#else
class Timer : public KQTimer
{
public:
  using KQTimer::KQTimer;
};
#endif

} // namespace bswi::event
