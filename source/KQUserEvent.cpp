/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQUserEvent Class
 * @details   KQueue based UserEvent implementation
 *-
 */

#include "KQUserEvent.h"

using std::function;
using std::string;

namespace bswi::event
{

KQUserEvent::KQUserEvent(const string &name, const function<bool()> &callback, Priority priority)
: IUserEvent(name, callback, priority)
{
  lateSetup();
}

KQUserEvent::KQUserEvent(const string &name, Priority priority)
: IUserEvent(name, priority)
{
  lateSetup();
}

void KQUserEvent::lateSetup()
{
  setDispatch([this]() {
    if (m_callback != nullptr) {
      return m_callback();
    }
    return true;
  });

  setFinalize([this]() {
    if (m_keventDescriptor.kq != -1) {
      m_keventDescriptor.ev.flags = EV_DELETE;
      kevent(m_keventDescriptor.kq, &m_keventDescriptor.ev, 1, nullptr, 0, nullptr);
    }
  });

  setTrigger([this]() {
    int status = -1;

    if (m_keventDescriptor.kq != -1) {
      m_keventDescriptor.ev.flags = EV_ADD;
      m_keventDescriptor.ev.fflags = NOTE_TRIGGER;
      status = kevent(m_keventDescriptor.kq, &m_keventDescriptor.ev, 1, nullptr, 0, nullptr);
    }

    return (status >= 0);
  });

  m_keventDescriptor.kq = -1;
  m_keventDescriptor.fd = reinterpret_cast<uintptr_t>(this);
  m_keventDescriptor.enable = true;

  EV_SET(&m_keventDescriptor.ev, m_keventDescriptor.fd, EVFILT_USER, 0, NOTE_FFNOP, 0, nullptr);
  m_descriptor = std::make_any<KEventDescriptor *>(&m_keventDescriptor);
}

} // namespace bswi::event
