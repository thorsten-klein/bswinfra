project(BSWInfra)

set(LIB_SRC
		Application.cpp
        KeyFile.cpp
		Logger.cpp)

if (${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
	set(LIB_SRC
		EPEventLoop.cpp
		EPTimer.cpp
		EPUserEvent.cpp
		EPInotify.cpp
		EPPollable.cpp
		${LIB_SRC})
else()
	set(LIB_SRC
		KQTimer.cpp
		KQEventLoop.cpp
		KQUserEvent.cpp
		KQPathEvent.cpp
		KQPollable.cpp
		${LIB_SRC})
endif()

find_package          (PkgConfig REQUIRED)
find_package		  (Threads REQUIRED)
set                   (LIBS ${LIBS} pthread)

add_library(${PROJECT_NAME} STATIC ${LIB_SRC})
target_link_libraries(${PROJECT_NAME} PUBLIC ${LIBS})
target_include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
