/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPEventLoop Class
 * @details   EPoll based EventLoop implementation class
 *-
 */

#pragma once

#include <condition_variable>
#include <mutex>

#include "IEventLoop.h"

namespace bswi::event
{

class EPEventLoop : public IEventLoop
{
public:
  explicit EPEventLoop(const std::string &name, std::size_t poll_usec, bool own_thread = true);
  virtual ~EPEventLoop();

  int addSource(const std::shared_ptr<IEventSource> source, IEventSource::Priority priority) final;

  int removeSource(const std::shared_ptr<IEventSource> source) final;
  int removeSource(const std::string &sourceName) final;
  void triggerSource(const std::string &sourceName) final;
  void start() final;
  void stop() final;

public:
  void setStarted(bool state) { m_started = state; }

  bool isStarted() { return m_started; }

  [[nodiscard]] int getEpollFD() const { return m_epollFd; }
  auto runThreadCond() -> std::condition_variable & { return m_runThreadCond; }
  auto runThreadMutex() -> std::mutex & { return m_runThreadMutex; }

private:
  std::condition_variable m_runThreadCond;
  std::mutex m_runThreadMutex;
  std::atomic<bool> m_started = false;
  int m_epollFd = -1;
};

} // namespace bswi::event
