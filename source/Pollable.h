/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Pollable Class
 * @details   Pollable wrapper class
 *-
 */

#pragma once

#if defined(__linux__)
#include "EPPollable.h"
#else
#include "KQPollable.h"
#endif

namespace bswi::event
{

#if defined(__linux__)
class Pollable : public EPPollable
{
public:
  using EPPollable::EPPollable;
};
#else
class Pollable : public KQPollable
{
public:
  using KQPollable::KQPollable;
};
#endif

} // namespace bswi::event
