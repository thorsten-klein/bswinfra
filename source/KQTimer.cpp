/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQTimer Class
 * @details   KQueue based Timer implementation
 *-
 */

#include "KQTimer.h"

namespace bswi::event
{

using std::function;
using std::string;

KQTimer::KQTimer(const string &name, const function<bool()> &callback, Priority priority)
: ITimer(name, callback, priority)
{
  m_keventDescriptor.kq = -1;
  m_keventDescriptor.fd = reinterpret_cast<uintptr_t>(this);
  m_keventDescriptor.enable = false;

  EV_SET(&m_keventDescriptor.ev, m_keventDescriptor.fd, EVFILT_TIMER, 0, 0, 0, nullptr);
  m_descriptor = std::make_any<KEventDescriptor *>(&m_keventDescriptor);
}

KQTimer::~KQTimer()
{
  stop();
}

auto KQTimer::start(uint64_t usec, bool repeat) -> int
{
  int status = 0;

  m_keventDescriptor.ev.data = usec;
  m_keventDescriptor.ev.fflags = NOTE_USECONDS;

  if (!repeat) {
    m_keventDescriptor.ev.fflags |= EV_ONESHOT;
  }

  if (m_keventDescriptor.kq != -1) {
    m_keventDescriptor.ev.flags = EV_ADD | EV_CLEAR;
    status = kevent(m_keventDescriptor.kq, &m_keventDescriptor.ev, 1, nullptr, 0, nullptr);
  } else {
    // We let event loop to start the timer when added
    m_keventDescriptor.enable = true;
  }

  if (status >= 0) {
    m_armed = true;
  }

  return status;
}

auto KQTimer::stop() -> int
{
  int status = 0;

  if (!m_armed) {
    return 0;
  }

  if (m_keventDescriptor.kq != -1) {
    m_keventDescriptor.ev.flags = EV_DELETE;
    status = kevent(m_keventDescriptor.kq, &m_keventDescriptor.ev, 1, nullptr, 0, nullptr);
  }

  m_armed = false;

  return status;
}

} // namespace bswi::event
