/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPInotify Class
 * @details   INotify based PathEvent implementation
 *-
 */

#pragma once

#include <map>
#include <mutex>
#include <string>

#include "EPTypes.h"
#include "IPathEvent.h"

namespace bswi::event
{

class EPInotify : public IPathEvent
{
public:
  explicit EPInotify(
      const std::string &name,
      const std::function<bool(const std::string &path, unsigned eventMask)> &callback,
      unsigned flags,
      Priority priority = Priority::Normal);
  virtual ~EPInotify();

  auto addWatch(const std::string &path, unsigned eventMask) -> int final;
  auto remWatch(const std::string &path) -> int final;

private:
  std::mutex m_watchesMutex;
  std::map<std::string, int> m_watches;
  EPollDescriptor m_epollDescriptor = {};
};

} // namespace bswi::event
