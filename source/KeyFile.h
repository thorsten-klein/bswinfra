/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KeyFile Class
 * @details   INI style key/value parser class
 *-
 */

#pragma once

#include <fstream>
#include <memory>
#include <optional>
#include <string>
#include <utility>
#include <vector>

namespace bswi::kf
{

struct Property {
  std::string key;
  std::string value;

  Property(const Property &orig) = default;
  Property(std::string withKey, std::string withValue)
  : key(std::move(withKey))
  , value(std::move(withValue)){};
};

struct Section {
  std::vector<Property> properties;
  std::string name;
  int id;

  Section(const Section &orig) = default;
  Section(std::string withName, int withId)
  : name(std::move(withName))
  , id(withId){};

  std::optional<Property> getProperty(const std::string &key);
  std::optional<std::string> getValue(const std::string &key);
};

class KeyFile
{
public:
  explicit KeyFile(std::string filePath)
  : m_inputFile(std::move(filePath)){};

  auto parseFile() -> int;
  void printStdOutput();
  auto getSections(const std::string &sectionName) -> std::vector<Section>;
  auto getSections() -> const std::vector<Section> &;
  auto getSection(const std::string &sectionName, int id) -> std::optional<Section>;
  auto getProperties(const std::string &sectionName, int id) -> std::vector<Property>;
  auto getProperty(const std::string &sectionName, int id, const std::string &key)
      -> std::optional<Property>;
  auto getPropertyValue(const std::string &sectionName, int id, const std::string &key)
      -> std::optional<std::string>;

protected:
  int parseLine(const std::string &line);

private:
  std::string m_inputFile;
  std::ifstream m_inputStream;
  std::vector<Section> m_sections;
};

} // namespace bswi::kf
