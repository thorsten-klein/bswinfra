/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQTypes Declarations
 * @details   Common data types for KQueue based classes
 *-
 */

#pragma once

#include <ctime>
#include <sys/event.h>
#include <sys/types.h>

namespace bswi::event
{

typedef struct _keventDescriptor {
  struct kevent ev;
  bool enable;
  uintptr_t fd;
  int kq;
} KEventDescriptor;

} // namespace bswi::event
