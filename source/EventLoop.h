/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EventLoop Class
 * @details   EPoll based EventLoop wrapper class
 *-
 */

#pragma once

#if defined(__linux__)
#include "EPEventLoop.h"
#else
#include "KQEventLoop.h"
#endif

namespace bswi::event
{

#if defined(__linux__)
class EventLoop : public EPEventLoop
{
public:
  using EPEventLoop::EPEventLoop;
};
#else
class EventLoop : public KQEventLoop
{
public:
  using KQEventLoop::KQEventLoop;
};
#endif

} // namespace bswi::event
