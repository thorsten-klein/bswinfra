/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPPollable Class
 * @details   EPoll based Pollable implementation
 *-
 */

#include "EPPollable.h"

using std::function;
using std::string;

namespace bswi::event
{

auto pollableEventsToEpollEvents(int events) -> int
{
  int epollEvents = 0;

  if ((events & IPollable::Events::Level) == IPollable::Events::Level) {
    epollEvents |= static_cast<int>(EPOLLIN);
  }
  if ((events & IPollable::Events::Edge) == IPollable::Events::Edge) {
    epollEvents |= static_cast<int>(EPOLLET);
  }
  if ((events & IPollable::Events::OneShot) == IPollable::Events::OneShot) {
    epollEvents |= static_cast<int>(EPOLLONESHOT);
  }
  if ((events & IPollable::Events::Wakeup) == IPollable::Events::Wakeup) {
    epollEvents |= static_cast<int>(EPOLLWAKEUP);
  }

  return epollEvents;
}

EPPollable::EPPollable(
    const string &name, const function<bool()> &callback, int fd, int events, Priority priority)
: IPollable(name, callback, fd, events, priority)
{
  m_epollDescriptor.fd = m_fd;
  m_epollDescriptor.ev.data.fd = m_epollDescriptor.fd;
  m_epollDescriptor.ev.events = pollableEventsToEpollEvents(m_events);
  m_descriptor = &m_epollDescriptor;
}

void EPPollable::lateSetup(const std::function<bool()> &callback,
                           int fd,
                           int events,
                           Priority priority)
{
  IPollable::lateSetup(callback, fd, events, priority);

  m_epollDescriptor.fd = m_fd;
  m_epollDescriptor.ev.data.fd = m_epollDescriptor.fd;
  m_epollDescriptor.ev.events = pollableEventsToEpollEvents(m_events);
  m_descriptor = std::make_any<EPollDescriptor *>(&m_epollDescriptor);
}

} // namespace bswi::event
