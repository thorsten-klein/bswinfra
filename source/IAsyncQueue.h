/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra IAsyncQueue Class
 * @details   AsyncQueue interface class
 *-
 */

#pragma once

#include <atomic>

#include "IEventSource.h"

namespace bswi::event
{

template <class T>
class IAsyncQueue : public IEventSource
{
public:
  explicit IAsyncQueue(const std::string &name,
                       const std::function<bool(const T &)> &,
                       Priority priority = Priority::Normal)
  {
    m_name = name;
    m_priority = priority;
    setPrepare([]() { return true; });
    setCheck([](int) { return true; });
  }

  virtual auto push(const T &val) -> bool = 0;
};

} // namespace bswi::event
