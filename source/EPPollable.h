/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPPollable Class
 * @details   EPoll based Pollable implementation
 *-
 */

#pragma once

#include "EPTypes.h"
#include "IPollable.h"

namespace bswi::event
{

class EPPollable : public IPollable
{
public:
  explicit EPPollable(const std::string &name,
                      const std::function<bool()> &callback,
                      int fd,
                      int events,
                      Priority priority = Priority::Normal);
  explicit EPPollable(const std::string &name)
  : IPollable(name)
  {
  }

  virtual ~EPPollable() = default;

  void
  lateSetup(const std::function<bool()> &callback, int fd, int events, Priority priority) final;

private:
  EPollDescriptor m_epollDescriptor = {};
};

} // namespace bswi::event
