/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra IEventSource Class
 * @details   EventSource interface class
 *-
 */

#pragma once

#include <any>
#include <functional>
#include <string>

namespace bswi::event
{

class IEventSource
{
public:
  enum class Priority { Normal, High };

public:
  const std::string &getName() { return m_name; }
  void setName(const std::string &name) { m_name = name; }

  auto getPriority() -> Priority { return m_priority; }

  void setPriority(Priority priority) { m_priority = priority; }

  std::any getDescriptor() { return m_descriptor; }

  void setReady(bool state) { m_ready = state; }

  [[nodiscard]] bool isReady() const { return m_ready; }

  void setPolled(bool state) { m_polled = state; }

  [[nodiscard]] bool isPolled() const { return m_polled; }

  bool prepare()
  {
    if (m_prepare != nullptr) {
      return m_prepare();
    }
    return true;
  }

  bool check(int events)
  {
    if (m_check != nullptr) {
      return m_check(events);
    }
    return true;
  }

  bool dispatch()
  {
    if (m_dispatch != nullptr) {
      return m_dispatch();
    }
    return false;
  }

  void finalize()
  {
    if (m_finalize != nullptr) {
      m_finalize();
    }
  }

  bool trigger()
  {
    if (m_trigger != nullptr) {
      return m_trigger();
    }
    return false;
  }

protected:
  [[maybe_unused]] void setPrepare(const std::function<bool()> &callback) { m_prepare = callback; }

  [[maybe_unused]] void setCheck(const std::function<bool(int)> &callback) { m_check = callback; }

  [[maybe_unused]] void setDispatch(const std::function<bool()> &callback)
  {
    m_dispatch = callback;
  }

  [[maybe_unused]] void setFinalize(const std::function<void()> &callback)
  {
    m_finalize = callback;
  }

  [[maybe_unused]] void setTrigger(const std::function<bool()> &callback) { m_trigger = callback; }

protected:
  Priority m_priority = Priority::Normal;
  std::string m_name = "noname";
  std::any m_descriptor;
  bool m_ready = false;  // Will only be updated on event loop
  bool m_polled = false; // Will only be updated on event loop

private:
  std::function<bool()> m_prepare = nullptr;         // Called before event sources are polled
  std::function<bool(int events)> m_check = nullptr; // Called after events sources are polled
  std::function<bool()> m_dispatch = nullptr;        // Called to dispatch the event source
  std::function<void()> m_finalize = nullptr;        // Called after event source is destroyed
  std::function<bool()> m_trigger = nullptr;         // Use to trigger event source explicitly
};

} // namespace bswi::event
