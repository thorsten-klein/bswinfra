/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Logger Class
 * @details   Logger class with build time backend selection
 *-
 */

#include "Logger.h"
#ifdef WITH_SYSLOG
#include <syslog.h>
#endif
#ifdef WITH_JOURNALD
#include <systemd/sd-journal.h>
#endif

#include <iostream>

using std::string;

namespace bswi::log
{

Logger *Logger::instance = nullptr;
string Logger::m_appId;

void printConsole(Logger::Message::Type type, const string &appId, const string &pld)
{
  string level;

  switch (type) {
  case Logger::Message::Type::Verbose:
    level = "Verbose";
    break;
  case Logger::Message::Type::Debug:
    level = "Debug  ";
    break;
  case Logger::Message::Type::Info:
    level = "Info   ";
    break;
  case Logger::Message::Type::Notice:
    level = "Notice ";
    break;
  case Logger::Message::Type::Warn:
    level = "Warning";
    break;
  case Logger::Message::Type::Error:
    level = "Error  ";
    break;
  case Logger::Message::Type::Fatal:
    level = "Fatal  ";
    break;
  }

  std::cout << appId << "|" << level << "\t| " << pld << '\n';
}

#ifdef WITH_SYSLOG
void printSyslog(Logger::Message::Type type, const string &, const string &pld)
{
  int level = LOG_INFO;

  switch (type) {
  case Logger::Message::Type::Verbose:
  case Logger::Message::Type::Debug:
    level = LOG_DEBUG;
    break;
  case Logger::Message::Type::Info:
    level = LOG_INFO;
    break;
  case Logger::Message::Type::Notice:
    level = LOG_NOTICE;
    break;
  case Logger::Message::Type::Warn:
    level = LOG_WARNING;
    break;
  case Logger::Message::Type::Error:
    level = LOG_ERR;
    break;
  case Logger::Message::Type::Fatal:
    level = LOG_ALERT;
    break;
  }

  syslog(level, "%s", pld.c_str());
}
#endif
#ifdef WITH_JOURNALD
void printJournald(Logger::Message::Type type, const string &, const string &pld)
{
  int level = LOG_INFO;

  switch (type) {
  case Logger::Message::Type::Verbose:
  case Logger::Message::Type::Debug:
    level = LOG_DEBUG;
    break;
  case Logger::Message::Type::Info:
    level = LOG_INFO;
    break;
  case Logger::Message::Type::Notice:
    level = LOG_NOTICE;
    break;
  case Logger::Message::Type::Warn:
    level = LOG_WARNING;
    break;
  case Logger::Message::Type::Error:
    level = LOG_ERR;
    break;
  case Logger::Message::Type::Fatal:
    level = LOG_ALERT;
    break;
  }

  sd_journal_print(level, "%s", pld.c_str());
}
#endif

void Logger::Message::print()
{
#ifdef WITH_SYSLOG
  printSyslog(m_type, m_appId, m_stream.str());
#elif WITH_JOURNALD
  printJournald(m_type, m_appId, m_stream.str());
#else
  printConsole(m_type, m_appId, m_stream.str());
#endif
}

void Logger::registerApplication(const string &appId, const string & /*description*/)
{
  m_appId = appId;
#ifdef WITH_SYSLOG
  openlog(m_appId.c_str(), LOG_PID, LOG_USER);
#else
  logInfo() << "Logging open for " + appId;
#endif
}

Logger::~Logger()
{
  logInfo() << "Logging closed for " + m_appId;
}

} // namespace bswi::log
