/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra KQUserEvent Class
 * @details   KQueue based UserEvent implementation
 *-
 */

#pragma once

#include "IUserEvent.h"
#include "KQTypes.h"

namespace bswi::event
{

class KQUserEvent : public IUserEvent
{
public:
  explicit KQUserEvent(const std::string &name,
                       const std::function<bool()> &callback,
                       Priority priority = Priority::Normal);

  explicit KQUserEvent(const std::string &name, Priority priority = Priority::Normal);

  virtual ~KQUserEvent() = default;

private:
  void lateSetup();

private:
  KEventDescriptor m_keventDescriptor = {};
};

} // namespace bswi::event
