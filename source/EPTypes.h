/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPTypes Declarations
 * @details   Common data types for EPoll based classes
 *-
 */

#pragma once

#include <sys/epoll.h>

namespace bswi::event
{

typedef struct _epollDescriptor {
  struct epoll_event ev;
  int fd;
} EPollDescriptor;

} // namespace bswi::event
