/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra EPInotify Class
 * @details   INotify based PathEvent implementation
 *-
 */

#include "EPInotify.h"
#include "Exceptions.h"

#include <sys/inotify.h>
#include <unistd.h>

#include <cerrno>
#include <climits>
#include <cstdlib>

#include "Logger.h"

using std::function;
using std::pair;
using std::string;

namespace bswi::event
{

using Event = IPathEvent::Events;
using Flag = IPathEvent::Flags;

auto PE2IE(unsigned events) -> unsigned
{
  unsigned inotifyEvents = 0;

  if ((events & Event::Access) == Event::Access) {
    inotifyEvents |= IN_ACCESS;
  }
  if ((events & Event::Attrib) == Event::Attrib) {
    inotifyEvents |= IN_ATTRIB;
  }
  if ((events & Event::Close) == Event::Close) {
    inotifyEvents |= IN_CLOSE;
  }
  if ((events & Event::CloseWrite) == Event::CloseWrite) {
    inotifyEvents |= IN_CLOSE_WRITE;
  }
  if ((events & Event::CloseNoWrite) == Event::CloseNoWrite) {
    inotifyEvents |= IN_CLOSE_NOWRITE;
  }
  if ((events & Event::Create) == Event::Create) {
    inotifyEvents |= IN_CREATE;
  }
  if ((events & Event::Delete) == Event::Delete) {
    inotifyEvents |= IN_DELETE;
  }
  if ((events & Event::DeleteSelf) == Event::DeleteSelf) {
    inotifyEvents |= IN_DELETE_SELF;
  }
  if ((events & Event::Modify) == Event::Modify) {
    inotifyEvents |= IN_MODIFY;
  }
  if ((events & Event::Move) == Event::Move) {
    inotifyEvents |= IN_MOVED_FROM;
    inotifyEvents |= IN_MOVED_TO;
  }
  if ((events & Event::MoveSelf) == Event::MoveSelf) {
    inotifyEvents |= IN_MOVE_SELF;
  }
  if ((events & Event::MovedFrom) == Event::MovedFrom) {
    inotifyEvents |= IN_MOVED_FROM;
  }
  if ((events & Event::MovedTo) == Event::MovedTo) {
    inotifyEvents |= IN_MOVED_TO;
  }
  if ((events & Event::Open) == Event::Open) {
    inotifyEvents |= IN_OPEN;
  }
  if ((events & Event::DontFollow) == Event::DontFollow) {
    inotifyEvents |= IN_DONT_FOLLOW;
  }
  if ((events & Event::ExclUnlink) == Event::ExclUnlink) {
    inotifyEvents |= IN_EXCL_UNLINK;
  }
  if ((events & Event::MaskAdd) == Event::MaskAdd) {
    inotifyEvents |= IN_MASK_ADD;
  }
  if ((events & Event::OneShot) == Event::OneShot) {
    inotifyEvents |= IN_ONESHOT;
  }
  if ((events & Event::OnlyDir) == Event::OnlyDir) {
    inotifyEvents |= IN_ONLYDIR;
  }

  return inotifyEvents;
}

auto IE2PE(unsigned events) -> unsigned
{
  unsigned pathEvents = 0;

  if ((events & IN_ACCESS) == IN_ACCESS) {
    pathEvents |= Event::Access;
  }
  if ((events & IN_ATTRIB) == IN_ATTRIB) {
    pathEvents |= Event::Attrib;
  }
  if ((events & IN_CLOSE) == IN_CLOSE) {
    pathEvents |= Event::Close;
  }
  if ((events & IN_CLOSE_WRITE) == IN_CLOSE_WRITE) {
    pathEvents |= Event::CloseWrite;
  }
  if ((events & IN_CLOSE_NOWRITE) == IN_CLOSE_NOWRITE) {
    pathEvents |= Event::CloseNoWrite;
  }
  if ((events & IN_CREATE) == IN_CREATE) {
    pathEvents |= Event::Create;
  }
  if ((events & IN_DELETE) == IN_DELETE) {
    pathEvents |= Event::Delete;
  }
  if ((events & IN_DELETE_SELF) == IN_DELETE_SELF) {
    pathEvents |= Event::DeleteSelf;
  }
  if ((events & IN_MODIFY) == IN_MODIFY) {
    pathEvents |= Event::Modify;
  }
  if ((events & IN_MOVE) == IN_MOVE) {
    pathEvents |= Event::Move;
  }
  if ((events & IN_MOVE_SELF) == IN_MOVE_SELF) {
    pathEvents |= Event::MoveSelf;
  }
  if ((events & IN_MOVED_FROM) == IN_MOVED_FROM) {
    pathEvents |= Event::MovedFrom;
  }
  if ((events & IN_MOVED_TO) == IN_MOVED_TO) {
    pathEvents |= Event::MovedTo;
  }
  if ((events & IN_OPEN) == IN_OPEN) {
    pathEvents |= Event::Open;
  }
  if ((events & IN_DONT_FOLLOW) == IN_DONT_FOLLOW) {
    pathEvents |= Event::DontFollow;
  }
  if ((events & IN_EXCL_UNLINK) == IN_EXCL_UNLINK) {
    pathEvents |= Event::ExclUnlink;
  }
  if ((events & IN_MASK_ADD) == IN_MASK_ADD) {
    pathEvents |= Event::MaskAdd;
  }
  if ((events & IN_ONESHOT) == IN_ONESHOT) {
    pathEvents |= Event::OneShot;
  }
  if ((events & IN_ONLYDIR) == IN_ONLYDIR) {
    pathEvents |= Event::OnlyDir;
  }

  return pathEvents;
}

auto PF2IF(unsigned flags) -> unsigned
{
  unsigned inotifyFlags = 0;

  if ((flags & Flag::NonBlock) == Flag::NonBlock) {
    flags |= IN_NONBLOCK;
  }
  if ((flags & Flag::CloseExec) == Flag::CloseExec) {
    flags |= IN_CLOEXEC;
  }

  return inotifyFlags;
}

[[maybe_unused]] auto IF2PF(unsigned flags) -> unsigned
{
  unsigned pathFlags = 0;

  if ((flags & IN_NONBLOCK) == IN_NONBLOCK) {
    flags |= Flag::NonBlock;
  }
  if ((flags & IN_CLOEXEC) == IN_CLOEXEC) {
    flags |= Flag::CloseExec;
  }

  return pathFlags;
}

EPInotify::EPInotify(const string &name,
                     const function<bool(const string &path, unsigned)> &callback,
                     unsigned flags,
                     Priority priority)
: IPathEvent(name, callback, flags, priority)
{
  setDispatch([this, callback]() {
    char buf[sizeof(struct inotify_event) + PATH_MAX]
        __attribute__((aligned(__alignof__(struct inotify_event))));
    const struct inotify_event *event;
    bool status = true;
    ssize_t sz;

    do {
      sz = read(m_epollDescriptor.fd, &buf, sizeof(buf));
      if (sz < 0 && errno != EAGAIN) {
        status = false;
      } else if (sz <= 0) {
        break; // no more events to read in noblock mode
      } else {
        for (char *ptr = buf; ptr < buf + sz; ptr += sizeof(struct inotify_event) + event->len) {
          event = reinterpret_cast<const struct inotify_event *>(ptr);
          for (auto &watch : m_watches) {
            if ((watch.second == event->wd) && (event->mask != IN_IGNORED)) {
              status = callback(watch.first, IE2PE(event->mask));
            }
          }
        }
      }
    } while (status);

    return status;
  }); // This is our event source dispatch function

  setFinalize([this]() {
    if (m_epollDescriptor.fd > 0) {
      close(m_epollDescriptor.fd);
      m_epollDescriptor.fd = -1;
    }
  });

  if ((m_epollDescriptor.fd = inotify_init1(static_cast<int>(PF2IF(flags)))) < 0) {
    throw bswi::except::CreateDescriptor();
  }

  m_epollDescriptor.ev.data.fd = m_epollDescriptor.fd;
  m_epollDescriptor.ev.events = EPOLLIN;
  m_descriptor = std::make_any<EPollDescriptor *>(&m_epollDescriptor);
}

EPInotify::~EPInotify()
{
  if (m_epollDescriptor.fd > 0) {
    close(m_epollDescriptor.fd);
  }
}

auto EPInotify::addWatch(const string &path, unsigned eventMask) -> int
{
  std::scoped_lock lk(m_watchesMutex);

  auto search = m_watches.find(path);
  if (search != m_watches.end()) {
    search->second = inotify_add_watch(m_epollDescriptor.fd, path.c_str(), PE2IE(eventMask));
  }

  if (inotify_add_watch(m_epollDescriptor.fd, path.c_str(), PE2IE(eventMask)) < 0) {
    return -1;
  }
  m_watches.insert(pair<string, int>(path, search->second));

  return search->second;
}

auto EPInotify::remWatch(const string &path) -> int
{
  int status = -1;

  std::scoped_lock lk(m_watchesMutex);
  auto search = m_watches.find(path);
  if (search != m_watches.end()) {
    status = inotify_rm_watch(m_epollDescriptor.fd, search->second);
    m_watches.erase(search->first);
  }

  return status;
}

} // namespace bswi::event
