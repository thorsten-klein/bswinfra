/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     BSWInfra Application Class
 * @details   Basic event based application class
 *-
 */

#pragma once

#include <atomic>
#include <cstdlib>
#include <string>

#include "IApplication.h"

#include "AsyncQueue.h"
#include "EventLoop.h"
#include "KeyFile.h"
#include "Logger.h"
#include "PathEvent.h"
#include "Pollable.h"
#include "Timer.h"
#include "UserEvent.h"

using namespace bswi::kf;
using namespace bswi::log;
using namespace bswi::event;

namespace bswi::app
{

class Application : public IApplication
{
public:
  explicit Application(const std::string &name, const std::string &description);

  static Application *getInstance()
  {
    return instance == nullptr ? instance = new Application(getenv("_"), "BSWI Application")
                               : instance;
  }

  void stop() final
  {
    if (m_running) {
      m_mainEventLoop.reset();
    }
  }

public:
  Application(Application const &) = delete;
  void operator=(Application const &) = delete;

private:
  static Application *instance;
};

} // namespace bswi::app

#define App() bswi::app::Application::getInstance()
