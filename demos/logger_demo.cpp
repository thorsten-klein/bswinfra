/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     Logger demo application
 * @details   Test application for Logger class
 *-
 */

#include "Logger.h"

#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <memory.h>

using namespace std;
using namespace bswi::log;

static void terminate(int signum)
{
  cout << "Received signal " << signum << endl;
  exit(EXIT_SUCCESS);
}

auto main(int argc, char **argv) -> int
{
  int c;
  bool help = false;
  const char *msg = nullptr;
  int long_index = 0;

  struct option longopts[] = {{"message", required_argument, nullptr, 'c'},
                              {"help", no_argument, nullptr, 'h'},
                              {nullptr, 0, nullptr, 0}};

  while ((c = getopt_long(argc, argv, "m:h", longopts, &long_index)) != -1) {
    switch (c) {
    case 'm':
      msg = optarg;
      break;
    case 'h':
      help = true;
      break;
    default:
      break;
    }
  }

  if (msg == nullptr) {
    help = true;
  }

  if (help) {
    cout << "logger_demo: A simple Logger demo\n\n";
    cout << "Usage: logger_demo [OPTIONS] \n\n";
    cout << "  General:\n";
    cout << "     --message, -m      <string> Message string\n";
    cout << "  Help:\n";
    cout << "     --help, -h                  Print this help\n\n";

    exit(EXIT_SUCCESS);
  }

  signal(SIGINT, terminate);
  signal(SIGTERM, terminate);

  Logger::getInstance()->registerApplication("Demo", "Simple demo application");

  logInfo() << "This is a info message " << msg;
  logNotice() << "This is a notice message " << msg;
  logDebug() << "This is a debug message " << string(msg);
  logError() << "This is a error message " << string(msg);

  return EXIT_SUCCESS;
}
