/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     KeyFile demo application
 * @details   Test application for KeyFile class
 *-
 */

#include "KeyFile.h"

#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <memory.h>

using namespace std;
using namespace bswi::kf;

static void terminate(int signum)
{
  cout << "Received signal " << signum << endl;
  exit(EXIT_SUCCESS);
}

auto main(int argc, char **argv) -> int
{
  int c;
  bool help = false;
  const char *confpath = nullptr;
  int long_index = 0;

  struct option longopts[] = {{"config", required_argument, nullptr, 'c'},
                              {"help", no_argument, nullptr, 'h'},
                              {nullptr, 0, nullptr, 0}};

  while ((c = getopt_long(argc, argv, "c:h", longopts, &long_index)) != -1) {
    switch (c) {
    case 'c':
      confpath = optarg;
      break;
    case 'h':
      help = true;
      break;
    default:
      break;
    }
  }

  if (confpath == nullptr) {
    help = true;
  }

  if (help) {
    cout << "keyfile_demo: A simple KeyFile demo\n\n";
    cout << "Usage: keyfile_demo [OPTIONS] \n\n";
    cout << "  General:\n";
    cout << "     --config, -c      <path>    Path to the configuration file\n";
    cout << "  Help:\n";
    cout << "     --help, -h                  Print this help\n\n";

    exit(EXIT_SUCCESS);
  }

  signal(SIGINT, terminate);
  signal(SIGTERM, terminate);

  unique_ptr<KeyFile> keyfile = make_unique<KeyFile>(KeyFile(string(confpath)));

  if (keyfile->parseFile() == 0) {
    keyfile->printStdOutput();
  } else {
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
