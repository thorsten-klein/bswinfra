/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     Events demo application
 * @details   Chain BSWI event classes in a small application
 *-
 */

#include "Application.h"

#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <filesystem>
#include <getopt.h>
#include <iostream>
#include <memory.h>

using namespace std;
using namespace bswi::app;

static void terminate(int signum)
{
  logInfo() << "Received signal " << signum;
  exit(EXIT_SUCCESS);
}

auto main(int argc, char **argv) -> int
{
  int c;
  bool help = false;
  const char *msg = nullptr;
  int long_index = 0;

  struct option longopts[] = {{"message", required_argument, nullptr, 'c'},
                              {"help", no_argument, nullptr, 'h'},
                              {nullptr, 0, nullptr, 0}};

  while ((c = getopt_long(argc, argv, "m:h", longopts, &long_index)) != -1) {
    switch (c) {
    case 'm':
      msg = optarg;
      break;
    case 'h':
      help = true;
      break;
    default:
      break;
    }
  }

  if (msg == nullptr) {
    help = true;
  }

  if (help) {
    cout << "events_demo: A simple event loop demo\n\n";
    cout << "Usage: events_demo [OPTIONS] \n\n";
    cout << "  General:\n";
    cout << "     --message, -m      <string> Message string\n";
    cout << "  Help:\n";
    cout << "     --help, -h                  Print this help\n\n";

    exit(EXIT_SUCCESS);
  }

  signal(SIGINT, terminate);
  signal(SIGTERM, terminate);

  shared_ptr<AsyncQueue<string>> asyncQueue =
      make_shared<AsyncQueue<string>>("AsyncQueue", [](const string &str) {
        logInfo() << "Processing async: " << str;
        return true;
      });

  const unsigned flags = IPathEvent::Flags::NonBlock;

  shared_ptr<PathEvent> pathEvent = make_shared<PathEvent>(
      "NewInTMP",
      [](const string &path, int events) {
        logInfo() << "FS event for: " << path << " Events:" << events;
        return true;
      },
      flags);

  pathEvent->addWatch("/tmp/test.txt", IPathEvent::Events::Modify);

  shared_ptr<UserEvent> event = make_shared<UserEvent>("UserEvent", [msg, asyncQueue]() {
    filesystem::path demoFile("/tmp/demo1.txt");

    logInfo() << "User event triggered " << msg;
    asyncQueue->push(msg);

    if (filesystem::exists(demoFile)) {
      filesystem::remove(demoFile);
    }

    return true;
  });

  shared_ptr<Timer> timer = make_shared<Timer>("TimerExample", [msg, event]() {
    logInfo() << "Timer ticked " << msg;
    return event->trigger();
  });

  timer->start(3000000, true);

  App()->addEventSource(timer);
  App()->addEventSource(pathEvent);
  App()->addEventSource(asyncQueue);
  App()->addEventSource(event, IEventSource::Priority::High);

  App()->run();

  return EXIT_SUCCESS;
}
